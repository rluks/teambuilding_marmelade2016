﻿using UnityEngine;
using System.Collections;

public class PickUpCrystal : MonoBehaviour
{

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Player")
        {
            GameManager.Instance.MainPlayer.PickupCrystal();
            Destroy(gameObject);
        }
    }
}
