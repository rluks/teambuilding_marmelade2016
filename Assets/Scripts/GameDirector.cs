﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class GameDirector : MonoBehaviour {

    void OnEnable()
    {
        //Tell our 'OnLevelFinishedLoading' function to start listening for a scene change as soon as this script is enabled.
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    void OnDisable()
    {
        //Tell our 'OnLevelFinishedLoading' function to stop listening for a scene change as soon as this script is disabled. Remember to always have an unsubscription for every delegate you subscribe to!
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        CurrentSceneName = scene.name;
       
        GameTexts.Instance.Map.TryGetValue(CurrentSceneName, out currentTexts);
    }

    public DialogueText dialogueText;
    string CurrentSceneName;
    bool isDialogueFinished;
    List<string> currentTexts;
    float timeScaleMultiplier = 100.0f;
    //static float DialogueDelay = 0;
    static float DialogueDelay = 2.5f;
    bool doContinue;

    // Use this for initialization
    void Start () {
        if ((GameManager.Instance.getRoomFinished() && SceneManager.GetActiveScene().name != "kancl0")
            || GameManager.Instance.MainPlayer.bossEngaged)
        {
            isDialogueFinished = true;
            doContinue = false;
            return;
        }
        isDialogueFinished = false;
        GameManager.Instance.SetGameDirector(this);
        PauseGame();
        doContinue = true;
        NextDialogueText();
	}

    // Update is called once per frame
    void Update () {
        if (isDialogueFinished && doContinue)
        {
            doContinue = false;
            ContinueGame();
        }
    }

    void PauseGame()
    {
        Time.timeScale = 1/timeScaleMultiplier; //0 nejde kvuli coroutinam
    }

    void ContinueGame()
    {
        Time.timeScale = 1.0f;
    }

    void NextDialogueText()
    {
        StartCoroutine(NextDialogueTextCor());
    }

    IEnumerator NextDialogueTextCor()
    {
        foreach (string dialogueLine in currentTexts)
        {
            dialogueText.PrintDialogue(dialogueLine);

            if(dialogueLine != "")
                yield return new WaitForSeconds(DialogueDelay/timeScaleMultiplier);

        }

        isDialogueFinished = true;
    }



    
}
