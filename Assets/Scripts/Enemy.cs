﻿using UnityEngine;
using System.Collections;
using System;

public enum StateAI
{
    Idle,
    Chase,
    Attack
};

public class Enemy : MonoBehaviour,IDamagable {

    public UnityEngine.AI.NavMeshAgent agent;
    public Transform target;
    public StateAI state = StateAI.Idle;
    public float Health = 30;

    public float reload;
    public bool shooter = true;
    public Transform spawn;
    public GameObject bulletObj;
    public GameObject meleetObj;
    public Animator anim;
    public bool SaveDestroy = false;

    public AudioClip attack;
    public AudioClip die;

    //public AudioSource audio;

    SpriteRenderer spriteRenderer;
    DeadSprite deadSprite;

    // Use this for initialization
    protected virtual void Start () {
       if(SaveDestroy)
        {
            if(GameManager.Instance.IsDestroyed(name))
            {
                Destroy(gameObject);
                return;
            }
        }
        if (!target)
        {
            if (GameManager.Instance.MainPlayer)
            {
                target = GameManager.Instance.MainPlayer.transform;
                state = StateAI.Chase;
            }
        }

        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        deadSprite = GetComponentInChildren<DeadSprite>();
    }

    // Update is called once per frame
    protected virtual void Update () {
	    if(Input.GetKeyDown(KeyCode.E))
        {
            SetTarget();
        }
        switch(state)
        {
            case StateAI.Idle:
                break;
            case StateAI.Attack:
                Attack();
                break;
            case StateAI.Chase:
                if (target)
                {
                    agent.SetDestination(target.position);
                }
                break;
        }
        if(agent.velocity.magnitude > 0.1  && !walk)
        {
            walk = true;
            if (anim)
                anim.SetBool("Walk", walk);
        }
        if(agent.velocity.magnitude < 0.1 && walk)
        {
            walk = false;
            if (anim)
                anim.SetBool("Walk", walk);
        }
	}

    bool walk = false;

    [ContextMenu("Go")]
    void SetTarget()
    {
        state = StateAI.Chase;
    }

    float lastDmg;
    virtual public void TakeDamage(float Damage)
    {
        Health -= Damage;
        if (Health <= 0)
        {
            if(SaveDestroy)
            {
                GameManager.Instance.RegistrDestroyed(name);
            }
            deadSprite.Dead();
            if(die)
            {
                AudioSource.PlayClipAtPoint(die, transform.position);
            }
            Destroy(gameObject);

        }

        lastDmg = Damage;
        StartCoroutine(ShowTakingDamage());
    }

    IEnumerator ShowTakingDamage()
    {
        Color orig = Color.white;//spriteRenderer.color;    
        spriteRenderer.color = Color.red;

        yield return new WaitForSeconds(0.1f + lastDmg/2000.0f); ;

        spriteRenderer.color = orig;
        yield return null;
    }

    void OnTriggerEnter(Collider col)
    {
        //IDamagable victim = col.GetComponent<IDamagable>();
        if (col.tag == "Player")
        {
            state = StateAI.Attack;
            if (target)
            {
                agent.isStopped = true;
                return;
            }
            target = col.transform;

        }
    }

  public virtual void OnTriggerExit(Collider col)
    {
       
        if (col.tag == "Player")
        {
            agent.isStopped = false;
            state = StateAI.Chase;
            return;
        }
    }

    public virtual void Attack()
    {
        if(!target)
        {
            target = GameManager.Instance.MainPlayer.transform;
            state = StateAI.Chase;
            if(!target)
            {
                state = StateAI.Idle;
            }
        }
        Vector3 targetOnSameHeight = target.position;
        targetOnSameHeight.y = transform.position.y;
        transform.LookAt(targetOnSameHeight);

        if (reload < Time.time)
        {
            if (shooter)
            {
                if (anim)
                    anim.SetTrigger("Attack");
                GameObject obj = (GameObject)Instantiate(bulletObj, spawn.position, spawn.rotation);
                Shoot bullet = obj.GetComponent<Shoot>();
                if(attack)
                {
                    AudioSource.PlayClipAtPoint(attack, transform.position);
                }
                if (bullet)
                {
                    //animator.SetTrigger("Attack");
                    bullet.TagOwner = tag;
                    bullet.ShootIt();
                    reload = Time.time + 1;
                }
            }
            else
            {
                if (anim)
                    anim.SetTrigger("Attack");
                if (attack)
                {
                    AudioSource.PlayClipAtPoint(attack, transform.position);
                }
                GameObject obj = (GameObject)Instantiate(meleetObj, spawn.position, spawn.rotation);
                LegMove sword = obj.GetComponent<LegMove>();

                if (sword)
                {
                    sword.transform.SetParent(spawn);
                    sword.SetTagToIgnor(tag);
                    reload = Time.time + 0.5f;
                }
            }
        }
    }
}
