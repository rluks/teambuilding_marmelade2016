﻿using UnityEngine;
using System.Collections;

public class SwordDamage : MonoBehaviour {

    public float Damage = 10;
    public string TagOwner = "Player";

    GameObject penetrating = null;
    void OnTriggerEnter(Collider col)
    {
        IDamagable victim = col.GetComponent<IDamagable>();
        if (penetrating == col.gameObject)
        {
            return;
        }
        if (victim != null && col.tag != TagOwner && !col.isTrigger)
        {
            penetrating = col.gameObject;
            victim.TakeDamage(Damage);
        }
    }
}
