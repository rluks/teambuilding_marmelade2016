﻿using UnityEngine;
using System.Collections;

public class LegMove : MonoBehaviour {

    public float StartOn = 0;
    public float Move = 2;
    public float speed = 1;
    public SwordDamage damage;

    float actualRotation;
	// Use this for initialization
	void Start () {
        StartCoroutine(Rotate());
	}
	
	// Update is called once per frame
	public void SetTagToIgnor(string name)
    {
        damage.TagOwner = name;
    }

    IEnumerator Rotate()
    {
        float timeFromsStart = 0;
        while(timeFromsStart < speed)
        {
            timeFromsStart += Time.deltaTime;
            //transform.localRotation = Quaternion.Euler( 0,0,Mathf.Lerp(StartOn, StartOn + Angle, timeFromsStart / speed));
            transform.localPosition = new Vector3(Mathf.Lerp(StartOn, StartOn + Move, timeFromsStart / speed),0, 0);
            yield return null;
        } 
        Destroy(gameObject);
    }
}
