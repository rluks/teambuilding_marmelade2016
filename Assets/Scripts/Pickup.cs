﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    public static string pickupSword = "sword";
    public static string pickupGun = "gun";
    public static string pickupHead = "head";
    public static string pickupAss = "ass";
    public static string pickupTorso = "torso";

    public string pickupType = pickupSword;

    // Use this for initialization
    void Start () {
	    if (GameManager.Instance.IsDestroyed(name))
        {
            Destroy(gameObject);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Player")
        {
            if (pickupType == pickupGun)
                col.gameObject.GetComponent<Player>().PickupGun();
            else if (pickupType == pickupSword)
                col.gameObject.GetComponent<Player>().PickupSword();
            else if (pickupType == pickupAss)
                Debug.Log("pickupass");//now doors wont open unless objects picked, ass is used against the boss behind closed doors
            else if (pickupType == pickupHead)
            {
                col.gameObject.GetComponent<Player>().PickupHead();
            }
            else if (pickupType == pickupTorso)
            {
                GameManager.Instance.LoadWin();
            }
            GameManager.Instance.RegistrDestroyed(name);
            Destroy(gameObject);
            GameManager.Instance.SetCanProgress();
        }
    }
}
