﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public static GameManager Instance;
    public Player MainPlayer;
    public UserInterface ui;
    string lastScene;
    GameDirector gameDirector;
    bool headCanTalk;
    System.Random randgen;

    public AudioClip audioKancl;
    public AudioClip audioSklad;
    public AudioClip audioDungeon;
    public AudioClip audioDungeonFinal;
    public AudioClip audioWin;

    public string[] weaponTypes =
    {
        "Noha",
        "Nic",
        "Kouzlo",
        "Dvě nohy",
    };

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
         DestroyedItems =new Dictionary<string, List<string>>();
         TempDestroyed =new List<string>();
    }
	// Use this for initialization
	void Start () {
        randgen = new System.Random();
        headCanTalk = true;
        DontDestroyOnLoad(gameObject);
        StartCoroutine(PlayerHeadTalk());

        bgAudio = GetComponent<AudioSource>();
    }

	
	// Update is called once per frame
	void Update () {

        if (MainPlayer && ui)
        {
            if (SceneManager.GetActiveScene().name == "dungeon_final" && !MainPlayer.bossEngaged)
                MainPlayer.bossEngaged = true;
        }
	}

    IEnumerator PlayerHeadTalk()
    {
        yield return new WaitForSeconds(5);
        while (true) {
            if (headCanTalk && MainPlayer.hasHead != 0 && ui.dialogueText.text.text.Length == 0)
            {
                headCanTalk = false;
            }
            else
            {
                yield return new WaitForSeconds(5);
                continue;
            }
            yield return new WaitForSeconds(randgen.Next(0, 10));
            var kecy = GameTexts.Instance.hlavaKecy;
            string kec = kecy[randgen.Next(0, kecy.Count)];
            ui.dialogueText.PrintDialogue(kec);

            yield return new WaitForSeconds(5);
            ui.dialogueText.PrintDialogue("");
            headCanTalk = true;
        }

    }

    public void SetPlayer(Player player)
    {
        if(!MainPlayer)
        {
            MainPlayer = player;
            DontDestroyOnLoad(player.gameObject);
        }
        else
        {
            Destroy(player.gameObject);
        }
    }

    public void SetUserInterface(UserInterface iface)
    {
        if (!ui)
        {
            ui = iface;
            DontDestroyOnLoad(iface.gameObject);
        }
        else
        {
            Destroy(iface.gameObject); 
        }
    }

    public void SetGameDirector(GameDirector director)
    {
        //gameDirector = director;
        while (!ui)
        {
            ;
        }
        director.dialogueText = ui.dialogueText;
    }

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        Invoke("HandleBackgroundMusic", 0.3f);
        LoadNextCollider[] doors = FindObjectsOfType(typeof(LoadNextCollider)) as LoadNextCollider[];
        foreach(LoadNextCollider door in doors)
        {
            if(door.nextSceneName == lastScene)
            {
                MainPlayer.transform.position = door.transform.parent.transform.position;
                break;
            }
        }

       
    }

    AudioSource bgAudio;

    private void HandleBackgroundMusic()
    {
        string currentScene = SceneManager.GetActiveScene().name;

        if (currentScene == "MenuScene")
            return;

        AudioClip newClip = null;
        switch (currentScene)
        {
            case "kancl0":
            case "kancl1":
                newClip = audioKancl;
                break;
            case "sklad0":
                newClip = audioSklad;
                break;
            case "dungeon1":
            case "dungeon2":
            case "dungeon3":
            case "dungeon4":
                newClip = audioDungeon;
                break;
            case "dungeon_final":
                newClip = audioDungeonFinal;
                break;
            case "win":
                newClip = audioWin;
                break;
            default:             
                break;
        }

        if(newClip != bgAudio.clip)
        {
            bgAudio.clip = newClip;
            bgAudio.Play();
        }       
    }

    Dictionary<string, List<string>> DestroyedItems;
    List<string> TempDestroyed;

    public void RegistrDestroyed(string name)
    {
        TempDestroyed.Add(name);
    }

    public bool IsDestroyed(string name)
    {
        return TempDestroyed.Contains(name);
    }


    public void GoThroughDoor(string nextSceneName)
    {
        lastScene = SceneManager.GetActiveScene().name;
        SceneManager.LoadScene(nextSceneName);

        if (DestroyedItems.ContainsKey(lastScene))
        {
            DestroyedItems[lastScene] = TempDestroyed;
        }
        else
        {
            DestroyedItems.Add(lastScene, TempDestroyed);
        }
        TempDestroyed = new List<string>();
        DestroyedItems.TryGetValue(nextSceneName, out TempDestroyed);
        if(TempDestroyed == null)
        {
           TempDestroyed = new List<string>();
        }
    }

    public void SetPlayerHitpoints(float newHp)
    {
        MainPlayer.hitpoints = newHp;
    }

    public void LoadWin()
    {
        Destroy(ui.gameObject);
        SceneManager.LoadScene("win");
    }

    public void SetCanProgress()
    {
        string current_scene = SceneManager.GetActiveScene().name;
        MainPlayer.levelComplete[current_scene] = true;
    }

    public bool getRoomFinished()
    {
        string current_scene = SceneManager.GetActiveScene().name;
        return MainPlayer.levelComplete[current_scene];
    }
}
