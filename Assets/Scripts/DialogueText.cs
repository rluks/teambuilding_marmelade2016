﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DialogueText : MonoBehaviour {

    public Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = str;
	}

    public void PrintDialogue(string nextText)
    {
        str = nextText;
    }
    private string str;
}

