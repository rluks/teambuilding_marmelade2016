﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BossFinal : Enemy {

    bool enableUpdate = false;
    public GameObject gate;
    public GameObject minion;
    public Vector3 position;
    List<GameObject> minions =new List<GameObject>();
    
    // Use this for initialization
    protected override void Start() 
    {
	    base.Start();
        state = StateAI.Idle;
        StartCoroutine(spawningMinions());
	}
	
	// Update is called once per frame
	 protected override void Update () {
        if(!enableUpdate)
        {
            agent.isStopped = true;
            return;
        }

        transform.Translate(position * Time.deltaTime * 2) ;
        //base.Update();
	}

    IEnumerator spawningMinions()
    {
        float time = 0;
        while (gate)
        {
            if (time < Time.time)
            {
                Vector3 temp = position;
                temp.x += Random.Range(-1f, 1f);
                temp.z += Random.Range(-1f, 1f);
                GameObject tempMinion = (GameObject)Instantiate(minion, temp, Quaternion.identity);
                minions.Add(tempMinion);
                //minions.RemoveAll(item => item == null);
                time = Time.time + 5;
            }
            yield return null;
        }

        for (int i = 0; i < minions.Count; i++)
        {
            Destroy(minions[i]);
        }
        state = StateAI.Idle;
        enableUpdate = true;
        position = Vector3.forward;
        StartCoroutine(GoRobot());

    }

    IEnumerator GoRobot()
    {
        anim.SetBool("Walk", true);
        yield return new WaitForSeconds(0.5f);
        position *= -1;
        Vector3 temp;
        while (true)
        {
            
            float delay = Random.Range(0, 1);
            yield return new WaitForSeconds(delay);
            temp = position;
            position = Vector3.zero;
            //bust
            //spawn diamantu
            anim.SetBool("Walk", false);
            for (int i = 0; i < 5; i++)
            {
                GameObject tempMinion = (GameObject)Instantiate(bulletObj, spawn.position, spawn.rotation);
                Shoot bullet = tempMinion.GetComponent<Shoot>();
                bullet.TagOwner = tag;
                bullet.ShootIt();
                if(attack)
                    AudioSource.PlayClipAtPoint(attack, transform.position);
                //yield return null;
                yield return new WaitForSeconds(0.5f);
            }
            anim.SetBool("Walk", true);
            position = temp;
            yield return new WaitForSeconds(1 - delay);
            position *= -1;
        }

    }

    public override void TakeDamage(float Damage)
    {
        base.TakeDamage(Damage / 100);
    }

}
