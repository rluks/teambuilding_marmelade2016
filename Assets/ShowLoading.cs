﻿using UnityEngine;
using UnityEngine.UI;

public class ShowLoading : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	

    public void ChangeText()
    {
        text.text = "Loading...";
    }
}
