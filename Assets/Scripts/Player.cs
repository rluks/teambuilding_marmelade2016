﻿using UnityEngine;
using System.Collections.Generic;

public interface IDamagable
{
    void TakeDamage(float Damage);
}

public class Player : MonoBehaviour,IDamagable {

    public float speed = 1;
    public Rigidbody rigi;
    public GameObject body;
    public Dictionary<string, bool> levelComplete;
    public Transform spawn;
    public Transform spawnMelee;
    public SpriteRenderer head;

    public int selected = -1;
    public GameObject[] Guns;
    public GameObject[] Swords;
    public Animator animator;
    public float hitpoints;

    public AudioClip fireball;
    public AudioClip[] fart;
    public AudioClip melee;

    public AudioClip yahoo;

    int hasSword;
    int hasGun;
    int hasAss;
    public int hasHead;

    bool walk = false;
    public bool bossEngaged = false;
    float reaload = 0;
    // Use this for initialization

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start () {
        hitpoints = 100;
        levelComplete = new Dictionary<string, bool>()
        {
            { "kancl0", true },  // no need to do anything there
            { "kancl1", false },
            { "sklad0", false },
            { "dungeon1", false },
            { "dungeon2", false },
            { "dungeon3", false },
            { "dungeon4", false },
            { "dungeon_final", false },
        };

        //GameManager.Instance.SetPlayer(this);
        head = GetComponentsInChildren<SpriteRenderer>()[1];
        head.enabled = false;

        hasGun = 0;
        hasSword = 0;
        hasHead = 0;

        //debug cheat
        //PickupSword();
        //PickupGun();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 direction = new Vector3();
        direction.x = Input.GetAxis("Horizontal");
        direction.z = Input.GetAxis("Vertical");
        direction.x = Mathf.Abs(direction.x) > 0.1f ? Mathf.Sign(direction.x) : 0;
        direction.z = Mathf.Abs(direction.z) > 0.1f ? Mathf.Sign(direction.z) : 0;
        rigi.MovePosition(transform.position + direction  * speed * Time.deltaTime);
        float angle;
        if (direction.x != 0 && direction.z != 0)
        {
            if (!walk)
            {
                walk = true;
                animator.SetBool("Walk", walk);
            }
            angle = direction.x > 0 ? 45 : 135;
            angle = direction.z > 0 ? angle * -1 : angle;
            body.transform.rotation = Quaternion.Euler(90, angle , 0);

        }
        else if (!(direction.x == 0 && direction.z == 0))
        {
            angle = Mathf.Acos(Vector2.Dot(Vector3.right, direction.normalized)) * Mathf.Rad2Deg;
            if (direction.z > 0)
            {
                angle -= 180;
            }
            body.transform.rotation = Quaternion.Euler(90, angle , 0);
            if (!walk)
            {
                walk = true;
                animator.SetBool("Walk", walk);
            }

        }
        else if(walk)
        {
                walk = false;
                animator.SetBool("Walk", walk);
        }   
            
        if (Input.GetButtonDown("Fire1") && Time.time > reaload)
        {
            walk = false;
            animator.SetBool("Walk", walk);
            if(selected > 2)
            {
                animator.SetTrigger("Attack");
            }
            if (selected > 1)
            {
                animator.SetTrigger("Attack");
                
            }
            else if (selected > -1)
            {
                animator.SetTrigger("Attack");
                Melee();
            }
        }

        if(Input.GetButtonDown("ChangeMelee1") && hasSword > 0)
        {
            ChangeMelee1();
        }
        if (Input.GetButtonDown("ChangeRange1") && hasGun > 0)
        {
            ChangeRange1();
        }
        //transform.Translate(direction);
    }

    private void ChangeMelee1()
    {
        selected = 0;
        animator.SetInteger("Item", 1);
        GameManager.Instance.ui.weaponType.text = "Leg";
    }

    private void ChangeRange1()
    {
        selected = 2;
        animator.SetInteger("Item", 2);
        GameManager.Instance.ui.weaponType.text = "Magic";
    }

    void IDamagable.TakeDamage(float Damage)
    {
        if (hitpoints >= 0) {
            hitpoints -= Damage;
            GameManager.Instance.ui.hitpointSlider.value = hitpoints; 
            if (!GameManager.Instance.ui.gameOverScreen.enabled && hitpoints <= 0)
            {
                GameManager.Instance.ui.GameOver();
            }
        }
    }

    public void Shoot(int type = 0)
    {
        for(int i = 0; i < 2*hasGun + hasAss; ++i)
        {
            GameObject obj = (GameObject)Instantiate(Guns[type], spawn.position, spawn.rotation);
            Shoot bullet = obj.GetComponent<Shoot>();
            if (bullet)
            {
                bullet.TagOwner = tag;
                bullet.ShootIt(hasGun);
                reaload = Time.time + 0.28f;
            }
            if(type == 1)
            {
                if (fart.Length > 0)
                {
                    AudioClip clip = fart[UnityEngine.Random.Range(0,(fart.Length)-1)];
                    AudioSource.PlayClipAtPoint(clip, transform.position);
                }
                numCrystal--;
                if(numCrystal <= 0)
                {
                    SwitchOnAss(false);
                }
                break;
            }
            else
            {
                if (fireball)
                    AudioSource.PlayClipAtPoint(fireball, transform.position);
            }
        }
    }

    public void Melee()
    {
        GameObject obj = (GameObject)Instantiate(Swords[selected], spawnMelee.position, spawnMelee.rotation);
        LegMove sword = obj.GetComponent<LegMove>();

        if (melee)
            AudioSource.PlayClipAtPoint(melee, transform.position);
        if (sword)
        {
            sword.damage.Damage *= hasSword;
            sword.transform.SetParent(spawnMelee);
            reaload = Time.time + 0.28f;
        }
    }

    public void PickupSword()
    {
        hasSword++;
        ChangeMelee1();
    }

    public void PickupGun()
    {
        hasGun++;
        ChangeRange1();
    }

    public void PickupHead()
    {
        hasHead = 1;
        head.enabled = true;

        AudioSource.PlayClipAtPoint(yahoo, transform.position);
    }

    int numCrystal = 0;
    public void PickupCrystal()
    {
        if(numCrystal == 0)
        {
            SwitchOnAss();
        }
        numCrystal++;
    }

    public void SwitchOnAss(bool eqiup = true)
    {
        if (eqiup)
        {

            animator.SetInteger("Item", 3);
            hasGun = 0;
            hasSword = 0;
            hasAss = 1;
            selected = 3;
        }
        else
        {

            animator.SetInteger("Item", 2);
            hasGun = 2;
            hasSword = 2;
            hasAss = 1;
            selected = 2;
        }

    }
}
