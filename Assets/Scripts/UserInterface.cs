﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class UserInterface : MonoBehaviour {

    public Slider hitpointSlider;
    public Text weaponType;
    public DialogueText dialogueText;
    public Canvas gameOverScreen;
    float timeScaleMultiplier = 10000.0f;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        //GameManager.Instance.SetUserInterface(this);
        dialogueText = GameObject.Find("DialogueText").GetComponent<DialogueText>();
        gameOverScreen = GameObject.Find("GameOverScreenWrapper").GetComponent<Canvas>();
    }

    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            SceneManager.LoadScene("MenuScene");
            Destroy(gameObject);
        }
    }

    public void GameOver()
    {
        gameOverScreen.enabled = true;
        PauseGame();
    }

    void PauseGame()
    {
        Time.timeScale = 1 / timeScaleMultiplier;
    }

    public void ContinueGame()
    {
        GameManager.Instance.SetPlayerHitpoints(100.0f);
        gameOverScreen.enabled = false;
        hitpointSlider.value = 100;
        Time.timeScale = 1.0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
