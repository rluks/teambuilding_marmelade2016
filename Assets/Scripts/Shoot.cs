﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

    public string TagOwner;
    public Rigidbody rigi;
    public float Power = 100;
    public float Damage = 20;
    public GameObject leaveItem;


	// Use this for initialization
	void Start () {
       
	}
	
	// Update is called once per frame

    public void ShootIt(float multiplier=1)
    {
        Damage *= 1.0f;//multiplier; //numbers instead of power - more projectiles with smaller damage
        float randRange = 0.15f;
        Vector3 randomizer = new Vector3(Random.Range(-randRange, randRange), Random.Range(-randRange, randRange), Random.Range(-randRange, randRange));
        rigi.AddForce((transform.right + randomizer) * Power);
    }

    bool penetrating = false;

    void OnTriggerEnter(Collider col)
    {
        IDamagable victim =  col.GetComponent<IDamagable>();
        if(penetrating)
        {
            return;
        }
        if( victim != null &&  col.tag != TagOwner && !col.isTrigger)
        {
            penetrating = true;
            victim.TakeDamage(Damage);

            if(leaveItem)
            {
                Instantiate(leaveItem, transform.position, transform.rotation);
            }
            Destroy(gameObject);
        }
    }
}
