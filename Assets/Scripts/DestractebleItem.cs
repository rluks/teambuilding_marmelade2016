﻿using UnityEngine;
using System.Collections;
using System;

public class DestractebleItem : MonoBehaviour, IDamagable {

    public float Health;
    public bool SaveDestroy = false;
    public AudioClip destructionClip;

    SpriteRenderer spriteRenderer;
    DeadSprite deadSprite;
   

    void Start()
    {
        if(SaveDestroy && GameManager.Instance.IsDestroyed(name))
        {
            Destroy(gameObject);
        }

        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        deadSprite = GetComponentInChildren<DeadSprite>();
    }

    protected virtual void DestroyMe()
    {
        if(destructionClip)
            AudioSource.PlayClipAtPoint(destructionClip, transform.position);
        Destroy(gameObject);
    }

    void IDamagable.TakeDamage(float Damage)
    {
        Health -= Damage;
        if(Health <= 0)
        {
            if(SaveDestroy)
            {
                GameManager.Instance.RegistrDestroyed(name);
            }

            if(deadSprite != null)
                deadSprite.Dead();
            DestroyMe();
        }

        StartCoroutine(ShowTakingDamage());
    }

    IEnumerator ShowTakingDamage()
    {
        Color orig = Color.white;// spriteRenderer.color;
        spriteRenderer.color = Color.red;

        yield return new WaitForSeconds(0.1f); ;

        spriteRenderer.color = orig;
        yield return null;
    }
}
