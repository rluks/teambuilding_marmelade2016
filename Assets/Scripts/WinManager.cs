﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class WinManager : MonoBehaviour {

    public Image img1;
    public Image img2;
    public Image img3;
    public Text credits;

    bool isOver = false;

	// Use this for initialization
	void Start () {
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        StartCoroutine(Sequence());
	}
	
	// Update is called once per frame
	void Update () {
        if (isOver)
        {
            Destroy(GameObject.Find("GameManager")); 
            Destroy(GameObject.Find("GameTexts"));

            SceneManager.LoadScene("MenuScene");
        }
	}

    IEnumerator Sequence()
    {
        yield return new WaitForSeconds(2.0f);
        img2.enabled = true;
        yield return new WaitForSeconds(2.0f);
        img3.enabled = true;
        yield return new WaitForSeconds(2.0f);
        credits.enabled = true;

        yield return new WaitForSeconds(10.0f);
        isOver = true;
    }
}
