﻿using UnityEngine;
using System.Collections;

public class DeadSprite : MonoBehaviour {

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Dead()
    {   
        foreach(var sr in gameObject.GetComponentsInChildren<SpriteRenderer>())
            sr.enabled = true;

        transform.parent = null;

        StartCoroutine(Cleanup());
    }

    IEnumerator Cleanup()
    {
        yield return new WaitForSeconds(10.0f);
        Destroy(gameObject);
    }
}
