﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadNextCollider : MonoBehaviour {

    public string nextSceneName;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            GameManager.Instance.GoThroughDoor(nextSceneName);
        }
    }
}
