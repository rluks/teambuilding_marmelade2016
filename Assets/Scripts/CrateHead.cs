﻿using UnityEngine;
using System.Collections;

public class CrateHead : DestractebleItem {
    protected override void DestroyMe()
    {
        var head = GameObject.Find("HlavaPickup");
        head.GetComponent<BoxCollider>().isTrigger = true;
        head.GetComponentInChildren<SpriteRenderer>().enabled = true;
        head.transform.SetParent(transform.parent);
        Destroy(gameObject);
    }
}
