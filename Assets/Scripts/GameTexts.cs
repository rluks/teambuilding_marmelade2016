﻿using UnityEngine;
using System.Collections.Generic;

public class GameTexts : MonoBehaviour {

    public static GameTexts Instance;
    List<string> currentLevelStrings;

    void Awake()
    {
        string co_name = "Pepa";
        string co2_name = "Faust";
        string me_name = "Me";
        string head_name = "Head of F.";
        string fart_name = "Giant Fa'rat";
        string skeleton_name = "Skeleton";
        string final_boss_name = "Tentaclos";

        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        Map = new Dictionary<string, List<string>>();

        currentLevelStrings = new List<string>();
        Map.Add("kancl0", currentLevelStrings);
        addToDialog(co_name, "Maan, I would eat a cow and still be hungry.");
        addToDialog(co_name, "Lets' go get some foodzies.");
        addToDialog(me_name, "Sure, mate. Have you seen " + co2_name + "? He may wanna come by.");
        addToDialog(co_name, "I think he is back there in the other room.");
        addToDialog(me_name, "I'll go grab him, bet he's hungry as hell.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("kancl1", currentLevelStrings);
        addToDialog(me_name, "What the freaking fudge happened here?");
        addToDialog(me_name, "That must be " + co2_name + "'s leg. I should probably keep it.");
        addToDialog(me_name, "Just for sure.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("sklad0", currentLevelStrings);
        addToDialog(head_name, "Help me, help me!");
        addToDialog(head_name, "I'm a poor head in a box.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("dungeon1", currentLevelStrings);
        addToDialog(head_name, "That'd be my right hand.");
        addToDialog(head_name, "I seriously need it. Oh, wait.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("dungeon2", currentLevelStrings);
        addToDialog(me_name, "Huh, another hand. May come in handy.");
        addToDialog(head_name, "Missing limb jokes are not funny.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("dungeon3", currentLevelStrings);
        addToDialog(skeleton_name, "Kill the rat and doodoo on its body!");
        addToDialog(fart_name, "Auch, auch!");
        addToDialog(me_name, "Nooooo, Fa'rat!");
        addToDialog(fart_name, me_name + ", I love you.");
        addToDialog(me_name, "*whispers* Huh, that's kinda gay.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("dungeon4", currentLevelStrings);
        addToDialog(me_name, "Is that your ass?");
        addToDialog(head_name, "There's a bunch of zombies and that's all you care about?");
        addToDialog(head_name, "But for the record, yes. And yes, I shave.");
        currentLevelStrings.Add("");

        currentLevelStrings = new List<string>();
        Map.Add("dungeon_final", currentLevelStrings);
        addToDialog(final_boss_name, "What, you here? Get back to your cubicle.");
        addToDialog(me_name, "Boss? Did you build this dungeon?");
        addToDialog(final_boss_name, "Well, no, we got this place rented.");
        addToDialog(final_boss_name, "I just wish the decoration textures didn't suck so much.");
        addToDialog(final_boss_name, "Anyway, yeh, uhm... You scum, get back to work!");
        addToDialog(me_name, "I was actually looking for " + co2_name + ".");
        addToDialog(final_boss_name, "What a lucky accident. I got his torso right over here.");
        addToDialog(me_name, "Huh. What happened to him?");
        addToDialog(final_boss_name, "His body split up from petting too many little puppies.");
        addToDialog(me_name, "You tore him to piceces, right?");
        addToDialog(final_boss_name, "Yes, his presentation did not contain number of slides on each slide.");
        addToDialog(me_name, "Well, that's embarassing.");
        addToDialog(head_name, "In my defence, I paid an Indian guy to do it.");
        addToDialog(me_name, "Anyway, time to finish this game and see the credits. Prepare to die.");
        currentLevelStrings.Add("");

        hlavaKecy = new List<string>();
        currentLevelStrings = hlavaKecy;
        addToDialog(head_name, "My nose itches.");
        addToDialog(head_name, "If nothing else, I'd make a nice sex toy now.");
        addToDialog(head_name, "Go get 'em, tiger.");
        addToDialog(head_name, "I miss my butt the second most.");
        addToDialog(head_name, "I feel naked.");
        addToDialog(head_name, "I have a bad feeling about this.");
        addToDialog(head_name, "If I had hands I would give you a high five.");
        addToDialog(head_name, "* whistling \"Head and shoulders\"*");
        addToDialog(head_name, "So... watched any good movies recently?");
        addToDialog(head_name, "Pretty creepy, huh?");
        addToDialog(head_name, "World is mine!");
        addToDialog(head_name, "All your base are belong to us!");
        addToDialog(head_name, "This area sucks donkey balls.");
        addToDialog(head_name, "Did you know: an average 26.5-year-old pooped out about 41,562 poops in their lifetime?");
        addToDialog(head_name, "*tries to facepalm*");
    }

    public Dictionary<string, List<string>> Map;
    public List<string> hlavaKecy;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    void addToDialog(string name, string dialog)
    {
        currentLevelStrings.Add(name + ": " + dialog);
    }
}
