﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class TheDoor : MonoBehaviour {

    public SpriteRenderer doorRender;
    public Sprite openDoor;
    public Sprite closeDoor;
    public AudioClip openClip;
    public AudioClip closeClip;

    bool open = false;

    void OnTriggerEnter(Collider other)
    {
        bool isPlayer = other.gameObject.tag == "Player";
        if (!isPlayer)
        {
            //print("Dude, " + other.gameObject.name + ", srsly, gtfo.");
            return;
        }
        if (canProgress(other.gameObject))
        {
            var colliders = GetComponentsInChildren<BoxCollider>();
            doorRender.sprite = openDoor;
            open = true;
            if (openClip)
            {
                AudioSource.PlayClipAtPoint(openClip, transform.position);
            }
            if (colliders.Length == 3)
            {
                
                Destroy(colliders[1]);
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        bool isPlayer = other.gameObject.tag == "Player";
        if (open && isPlayer)
        {
            if(closeClip)
            {
                AudioSource.PlayClipAtPoint(closeClip, transform.position);
            }
            doorRender.sprite = closeDoor;
            open = false;
        }
    }

    bool canProgress(GameObject player)
    {
        string scene_name = SceneManager.GetActiveScene().name;
        if (player.GetComponent<Player>().levelComplete[scene_name])
        {
            return true;
        }
        return false;
    }
}
